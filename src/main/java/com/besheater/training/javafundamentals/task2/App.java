package com.besheater.training.javafundamentals.task2;

import com.google.common.collect.Ordering;

import java.io.PrintStream;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

public class App {
    public static void main( String[] args ) {
        startApp(new Scanner(System.in), System.out);
    }

    public static void startApp(Scanner scanner, PrintStream out) {
        List<BigInteger> numbers;
        try {
            numbers = getNumbers(scanner, out);
        } catch (IllegalArgumentException ex) {
            out.println(ex.getMessage());
            return;
        }

        BigInteger shortestNum = getFirstShortestNumber(numbers);
        out.printf("The first shortest number = %d with a length of %d digits.\n",
                    shortestNum, getNumberLength(shortestNum));

        BigInteger longestNum = getFirstLongestNumber(numbers);
        out.printf("The first longest number = %d with a length of %d digits.\n",
                    longestNum, getNumberLength(longestNum));

        BigInteger numWithLowestUniqDigitsCount = getFirstNumberWithLowestUniqDigitsCount(numbers);
        out.printf("The first number with the lowest unique digits count = %d " +
                   "it has only %d unique digits.\n",
                    numWithLowestUniqDigitsCount,
                    getUniqDigitsCount(numWithLowestUniqDigitsCount));

        Optional<BigInteger> numWithAscendingDigits = getFirstNumberWithAscendingDigits(numbers);
        if (numWithAscendingDigits.isPresent()) {
            out.printf("The first number with digits in ascending order = %d\n",
                        numWithAscendingDigits.get());
        } else {
            out.println("There isn't any number with digits in ascending order.");
        }

        Optional<BigInteger> numWithUniqDigits = getFirstNumberWithUniqDigits(numbers);
        if (numWithUniqDigits.isPresent()) {
            out.printf("The first number with unique digits = %d\n", numWithUniqDigits.get());
        } else {
            out.println("There isn't any number with unique digits.");
        }

        out.println("That's all. Bye!");
    }

    public static List<BigInteger> getNumbers(Scanner scanner, PrintStream out) {
        out.println("Enter some integer numbers separated by space: " +
                    "(e.g. 5 4 -16 9 0 25871 -42)");
        String numbersStr = scanner.nextLine();
        try {
            return parseDecimalIntegerNumbers(numbersStr);
        } catch (IllegalArgumentException ex) {
            String message = "Some of your numbers are not valid. " +
                             "Please type more carefully next time. Bye!";
            throw new IllegalArgumentException(message, ex);
        }
    }

    public static List<BigInteger> parseDecimalIntegerNumbers(String numbersStr) {
        String[] numbers = numbersStr.trim().split("\\s+");
        return Arrays.stream(numbers).map(BigInteger::new)
                                     .collect(Collectors.toList());
    }

    public static BigInteger getFirstShortestNumber(List<BigInteger> numbers) {
        return numbers.stream().reduce(numbers.get(0), App::getShortestNumber);
    }

    public static BigInteger getFirstLongestNumber(List<BigInteger> numbers) {
        return numbers.stream().reduce(numbers.get(0), App::getLongestNumber);
    }

    public static BigInteger getShortestNumber(BigInteger first, BigInteger second) {
        return getNumberLength(first) <= getNumberLength(second) ? first : second;
    }

    public static BigInteger getLongestNumber(BigInteger first, BigInteger second) {
        return getNumberLength(first) >= getNumberLength(second) ? first : second;
    }

    public static Integer getNumberLength(BigInteger num) {
        return num.abs().toString().length();
    }

    public static BigInteger getFirstNumberWithLowestUniqDigitsCount(List<BigInteger> numbers) {
        return numbers.stream().reduce(numbers.get(0), App::getNumberWithLowestUniqDigitsCount);
    }

    public static BigInteger getNumberWithLowestUniqDigitsCount(BigInteger first,
                                                                BigInteger second) {
        return getUniqDigitsCount(first) <= getUniqDigitsCount(second) ? first : second;
    }

    public static Integer getUniqDigitsCount(BigInteger num) {
        List<Integer> digits = splitIntoDigits(num);
        return new HashSet<>(digits).size();
    }

    public static List<Integer> splitIntoDigits(BigInteger num) {
        String[] digits = num.abs().toString().split("");
        return Arrays.stream(digits).map(Integer::valueOf)
                                    .collect(Collectors.toList());
    }

    public static Optional<BigInteger> getFirstNumberWithAscendingDigits(List<BigInteger> numbers) {
        return numbers.stream().filter(App::areDigitsAscending)
                               .findFirst();
    }

    public static boolean areDigitsAscending(BigInteger num) {
        if (isAllDigitsTheSame(num)) {
            return false;
        }
        List<Integer> digits = splitIntoDigits(num);
        return Ordering.natural().isOrdered(digits);
    }

    public static boolean isAllDigitsTheSame(BigInteger num) {
        List<Integer> digits = splitIntoDigits(num);
        return digits.stream().distinct()
                              .limit(2)
                              .count() <= 1;
    }

    public static Optional<BigInteger> getFirstNumberWithUniqDigits(List<BigInteger> numbers) {
        return numbers.stream().filter(App::areAllDigitsUniq)
                               .findFirst();
    }

    public static boolean areAllDigitsUniq(BigInteger num) {
        List<Integer> allDigits = splitIntoDigits(num);
        Set<Integer> uniqDigits = new HashSet<>(allDigits);
        return allDigits.size() == uniqDigits.size();
    }
}