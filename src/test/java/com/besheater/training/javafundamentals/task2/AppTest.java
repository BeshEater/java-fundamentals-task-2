package com.besheater.training.javafundamentals.task2;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.math.BigInteger.valueOf;
import static org.junit.jupiter.api.Assertions.*;

public class AppTest {

    @Test
    public void parseIntegerNumbers_nullArguments_throwsException() {
        assertThrows(NullPointerException.class,
                () -> App.parseDecimalIntegerNumbers(null));
    }

    @Test
    public void parseIntegerNumbers_invalidInput_throwsException() {
        assertThrows(NumberFormatException.class,
                () -> App.parseDecimalIntegerNumbers(""));
        assertThrows(NumberFormatException.class,
                () -> App.parseDecimalIntegerNumbers(" "));
        assertThrows(NumberFormatException.class,
                () -> App.parseDecimalIntegerNumbers("4.2 598 148.3"));
        assertThrows(NumberFormatException.class,
                () -> App.parseDecimalIntegerNumbers("7fd8 63v d87"));
        assertThrows(NumberFormatException.class,
                () -> App.parseDecimalIntegerNumbers("67/8 487 3101"));
        assertThrows(NumberFormatException.class,
                () -> App.parseDecimalIntegerNumbers("10E2 1E3 74"));
    }

    @Test
    public void parseIntegerNumbers_validInput_returnsAllNumbers() {
        assertEquals(asList(15),
                App.parseDecimalIntegerNumbers("15"));
        assertEquals(asList(-3),
                App.parseDecimalIntegerNumbers("-3"));
        assertEquals(asList(14, 25, 0, 789, 1),
                App.parseDecimalIntegerNumbers("014 25 0 789 1"));
        assertEquals(asList(-784, 13, 0, -1),
                App.parseDecimalIntegerNumbers(" -784 13 -0 -1"));
        assertEquals(asList(1, 2, 89, -3),
                App.parseDecimalIntegerNumbers(" 1  2 89    -3  "));
    }

    @Test
    public void getFirstShortestNumber_nullArguments_ThrowsException() {
        assertThrows(NullPointerException.class,
                () -> App.getFirstShortestNumber(null));
    }

    @Test
    public void getFirstShortestNumber_nullValuesInCollection_ThrowsException() {
        assertThrows(NullPointerException.class,
                () -> App.getFirstShortestNumber(asList(null)));
        assertThrows(NullPointerException.class,
                () -> App.getFirstShortestNumber(asList(1, null, 85, -6)));
    }

    @Test
    public void getFirstShortestNumber_validInput_returnsFirstShortestNumber() {
        assertEquals(valueOf(3), App.getFirstShortestNumber(asList(3)));
        assertEquals(valueOf(-56), App.getFirstShortestNumber(asList(-56)));
        assertEquals(valueOf(0), App.getFirstShortestNumber(asList(45, 7896, 0, -910)));
        assertEquals(valueOf(13), App.getFirstShortestNumber(asList(13, 1478, 741)));
        assertEquals(valueOf(-741), App.getFirstShortestNumber(asList(1000, 1478, -741)));
    }

    @Test
    public void getFirstLongestNumber_nullArguments_ThrowsException() {
        assertThrows(NullPointerException.class,
                () -> App.getFirstLongestNumber(null));
    }

    @Test
    public void getFirstLongestNumber_nullValuesInCollection_ThrowsException() {
        assertThrows(NullPointerException.class,
                () -> App.getFirstLongestNumber(asList(null)));
        assertThrows(NullPointerException.class,
                () -> App.getFirstLongestNumber(asList(1, null, 85, -6)));
    }

    @Test
    public void getFirstLongestNumber_validInput_returnsFirstLongestNumber() {
        assertEquals(valueOf(2), App.getFirstLongestNumber(asList(2)));
        assertEquals(valueOf(-17), App.getFirstLongestNumber(asList(-17)));
        assertEquals(valueOf(7896), App.getFirstLongestNumber(asList(7896, 1, 0, -910)));
        assertEquals(valueOf(13), App.getFirstLongestNumber(asList(6, 13, 4)));
        assertEquals(valueOf(-74192), App.getFirstLongestNumber(asList(1070, 14, -74192)));
    }

    @Test
    public void getShortestNumber_nullArguments_ThrowsException() {
        assertThrows(NullPointerException.class, () -> App.getShortestNumber(null, BigInteger.ONE));
        assertThrows(NullPointerException.class, () -> App.getShortestNumber(BigInteger.ONE, null));
        assertThrows(NullPointerException.class, () -> App.getShortestNumber(null, null));
    }

    @Test
    public void getShortestNumber_validInput_returnsShortestNumber() {
        assertEquals(valueOf(4), App.getShortestNumber(valueOf(4), valueOf(1)));
        assertEquals(valueOf(3), App.getShortestNumber(valueOf(27), valueOf(3)));
        assertEquals(valueOf(0), App.getShortestNumber(valueOf(0), valueOf(-142)));
        assertEquals(valueOf(-173), App.getShortestNumber(valueOf(-173), valueOf(7415)));
        assertEquals(valueOf(-1), App.getShortestNumber(valueOf(-251), valueOf(-1)));
    }

    @Test
    public void getLongestNumber_nullArguments_ThrowsException() {
        assertThrows(NullPointerException.class, () -> App.getLongestNumber(null, BigInteger.ONE));
        assertThrows(NullPointerException.class, () -> App.getLongestNumber(BigInteger.ONE, null));
        assertThrows(NullPointerException.class, () -> App.getLongestNumber(null, null));
    }

    @Test
    public void getLongestNumber_validInput_returnsLongestNumber() {
        assertEquals(valueOf(2), App.getLongestNumber(valueOf(2), valueOf(7)));
        assertEquals(valueOf(27), App.getLongestNumber(valueOf(27), valueOf(3)));
        assertEquals(valueOf(7415), App.getLongestNumber(valueOf(-173), valueOf(7415)));
        assertEquals(valueOf(-142), App.getLongestNumber(valueOf(0), valueOf(-142)));
        assertEquals(valueOf(-251), App.getLongestNumber(valueOf(-251), valueOf(-1)));
    }

    @Test
    public void getNumberLength_nullArguments_ThrowsException() {
        assertThrows(NullPointerException.class, () -> App.getNumberLength(null));
    }

    @Test
    public void getNumberLength_validInput_returnsNumberLength() {
        assertEquals(1, App.getNumberLength(valueOf(6)));
        assertEquals(1, App.getNumberLength(valueOf(-9)));
        assertEquals(3, App.getNumberLength(valueOf(100)));
        assertEquals(5, App.getNumberLength(valueOf(12345)));
        assertEquals(4, App.getNumberLength(valueOf(-1000)));
    }

    @Test
    public void getFirstNumberWithLowestUniqDigitsCount_nullArguments_ThrowsException() {
        assertThrows(NullPointerException.class,
                () -> App.getFirstNumberWithLowestUniqDigitsCount(null));
    }

    @Test
    public void getFirstNumberWithLowestUniqDigitsCount_nullValuesInCollection_ThrowsException() {
        assertThrows(NullPointerException.class,
                () -> App.getFirstNumberWithLowestUniqDigitsCount(asList(null)));
        assertThrows(NullPointerException.class,
                () -> App.getFirstNumberWithLowestUniqDigitsCount(asList(1, null, 85, -6)));
    }

    @Test
    public void getFirstNumberWithLowestUniqDigitsCount_validInput_returnsWhatExpected() {
        assertEquals(valueOf(3),
                App.getFirstNumberWithLowestUniqDigitsCount(asList(3)));
        assertEquals(valueOf(-56),
                App.getFirstNumberWithLowestUniqDigitsCount(asList(-56)));
        assertEquals(valueOf(0),
                App.getFirstNumberWithLowestUniqDigitsCount(asList(45, 7896, 0, -910)));
        assertEquals(valueOf(13),
                App.getFirstNumberWithLowestUniqDigitsCount(asList(13, 112233, 741)));
        assertEquals(valueOf(-1001),
                App.getFirstNumberWithLowestUniqDigitsCount(asList(1002, 147, -1001)));
    }

    @Test
    public void getNumberWithLowestUniqDigitsCount_nullArguments_ThrowsException() {
        assertThrows(NullPointerException.class,
                () -> App.getNumberWithLowestUniqDigitsCount(null, BigInteger.ONE));
        assertThrows(NullPointerException.class,
                () -> App.getNumberWithLowestUniqDigitsCount(BigInteger.ONE, null));
        assertThrows(NullPointerException.class,
                () -> App.getNumberWithLowestUniqDigitsCount(null, null));
    }

    @Test
    public void getNumberWithLowestUniqDigitsCount_validInput_returnsWhatExpected() {
        assertEquals(valueOf(2),
                App.getNumberWithLowestUniqDigitsCount(valueOf(2), valueOf(7)));
        assertEquals(valueOf(333),
                App.getNumberWithLowestUniqDigitsCount(valueOf(27), valueOf(333)));
        assertEquals(valueOf(-173),
                App.getNumberWithLowestUniqDigitsCount(valueOf(-173), valueOf(7415)));
        assertEquals(valueOf(-141),
                App.getNumberWithLowestUniqDigitsCount(valueOf(-141), valueOf(141)));
        assertEquals(valueOf(100001),
                App.getNumberWithLowestUniqDigitsCount(valueOf(100001), valueOf(120000)));
    }

    @Test
    public void getUniqDigitsCount_nullArguments_ThrowsException() {
        assertThrows(NullPointerException.class, () -> App.getUniqDigitsCount(null));
    }

    @Test
    public void getUniqDigitsCount_validInput_returnsWhatExpected() {
        assertEquals(1, App.getUniqDigitsCount(valueOf(3)));
        assertEquals(1, App.getUniqDigitsCount(valueOf(88888)));
        assertEquals(2, App.getUniqDigitsCount(valueOf(100)));
        assertEquals(2, App.getUniqDigitsCount(valueOf(-17)));
        assertEquals(3, App.getUniqDigitsCount(valueOf(-12012)));
        assertEquals(5, App.getUniqDigitsCount(valueOf(12345)));
        assertEquals(6, App.getUniqDigitsCount(valueOf(-654321)));
    }

    @Test
    public void splitIntoDigits_nullArguments_ThrowsException() {
        assertThrows(NullPointerException.class, () -> App.splitIntoDigits(null));
    }

    @Test
    public void splitIntoDigits_validInput_returnsWhatExpected() {
        assertEquals(Arrays.asList(0), App.splitIntoDigits(valueOf(0)));
        assertEquals(Arrays.asList(3), App.splitIntoDigits(valueOf(3)));
        assertEquals(Arrays.asList(1, 4), App.splitIntoDigits(valueOf(14)));
        assertEquals(Arrays.asList(4, 0, 5, 0), App.splitIntoDigits(valueOf(4050)));
        assertEquals(Arrays.asList(7, 8, 4), App.splitIntoDigits(valueOf(-784)));
        assertEquals(Arrays.asList(1, 0, 0, 0), App.splitIntoDigits(valueOf(-1000)));
    }

    @Test
    public void getFirstNumberWithAscendingDigits_nullArguments_ThrowsException() {
        assertThrows(NullPointerException.class,
                () -> App.getFirstNumberWithAscendingDigits(null));
    }

    @Test
    public void getFirstNumberWithAscendingDigits_nullValuesInCollection_ThrowsException() {
        assertThrows(NullPointerException.class,
                () -> App.getFirstNumberWithAscendingDigits(asList(null)));
        assertThrows(NullPointerException.class,
                () -> App.getFirstNumberWithAscendingDigits(asList(1, null, 85, -6)));
    }

    @Test
    public void getFirstNumberWithAscendingDigits_validInput_returnsWhatExpected() {
        assertEquals(Optional.of(valueOf(23)),
                App.getFirstNumberWithAscendingDigits(asList(23)));
        assertEquals(Optional.of(valueOf(-56)),
                App.getFirstNumberWithAscendingDigits(asList(-56)));
        assertEquals(Optional.of(valueOf(-234)),
                App.getFirstNumberWithAscendingDigits(asList(54, 7896, -234, 0)));
        assertEquals(Optional.of(valueOf(12389)),
                App.getFirstNumberWithAscendingDigits(asList(74, 11230, 12389)));
        assertEquals(Optional.of(valueOf(12)),
                App.getFirstNumberWithAscendingDigits(asList(12, 1002, 147, -1001)));
        assertEquals(Optional.empty(),
                App.getFirstNumberWithAscendingDigits(asList(321)));
        assertEquals(Optional.empty(),
                App.getFirstNumberWithAscendingDigits(asList(-98)));
        assertEquals(Optional.empty(),
                App.getFirstNumberWithAscendingDigits(asList(120, -45671, 128345)));
        assertEquals(Optional.empty(),
                App.getFirstNumberWithAscendingDigits(asList(987, 21, 630, -74, -90)));
    }

    @Test
    public void areDigitsAscending_nullArguments_ThrowsException() {
        assertThrows(NullPointerException.class,
                () -> App.areDigitsAscending(null));
    }

    @Test
    public void areDigitsAscending_validInput_returnsWhatExpected() {
        assertTrue(App.areDigitsAscending(valueOf(15)));
        assertTrue(App.areDigitsAscending(valueOf(-89)));
        assertTrue(App.areDigitsAscending(valueOf(135)));
        assertTrue(App.areDigitsAscending(valueOf(-22239)));

        assertFalse(App.areDigitsAscending(valueOf(7)));
        assertFalse(App.areDigitsAscending(valueOf(0)));
        assertFalse(App.areDigitsAscending(valueOf(-1)));
        assertFalse(App.areDigitsAscending(valueOf(54)));
        assertFalse(App.areDigitsAscending(valueOf(33)));
        assertFalse(App.areDigitsAscending(valueOf(-111)));
        assertFalse(App.areDigitsAscending(valueOf(-998)));
        assertFalse(App.areDigitsAscending(valueOf(123406789)));
        assertFalse(App.areDigitsAscending(valueOf(-45566577)));
    }

    @Test
    public void getFirstNumberWithUniqDigits_nullArguments_ThrowsException() {
        assertThrows(NullPointerException.class,
                () -> App.getFirstNumberWithUniqDigits(null));
    }

    @Test
    public void getFirstNumberWithUniqDigits_nullValuesInCollection_ThrowsException() {
        assertThrows(NullPointerException.class,
                () -> App.getFirstNumberWithUniqDigits(asList(null)));
        assertThrows(NullPointerException.class,
                () -> App.getFirstNumberWithUniqDigits(asList(1, 14784, null, -6)));
    }

    @Test
    public void getFirstNumberWithUniqDigits_validInput_returnsWhatExpected() {
        assertEquals(Optional.of(valueOf(3)),
                App.getFirstNumberWithUniqDigits(asList(3)));
        assertEquals(Optional.of(valueOf(-56)),
                App.getFirstNumberWithUniqDigits(asList(-56)));
        assertEquals(Optional.of(valueOf(-234)),
                App.getFirstNumberWithUniqDigits(asList(544, 78976, -234, 0)));
        assertEquals(Optional.of(valueOf(12389)),
                App.getFirstNumberWithUniqDigits(asList(-774, 11230, 12389)));
        assertEquals(Optional.of(valueOf(0)),
                App.getFirstNumberWithUniqDigits(asList(0, 1001, 1477, -1001)));
        assertEquals(Optional.empty(),
                App.getFirstNumberWithUniqDigits(asList(22)));
        assertEquals(Optional.empty(),
                App.getFirstNumberWithUniqDigits(asList(-989)));
        assertEquals(Optional.empty(),
                App.getFirstNumberWithUniqDigits(asList(100, -456741, 3333)));
        assertEquals(Optional.empty(),
                App.getFirstNumberWithUniqDigits(asList(988, 11, 200, -717, -9888181)));
    }

    @Test
    public void areAllDigitsUniq_nullArguments_ThrowsException() {
        assertThrows(NullPointerException.class,
                () -> App.areAllDigitsUniq(null));
    }

    @Test
    public void areAllDigitsUniq_validInput_returnsWhatExpected() {
        assertTrue(App.areAllDigitsUniq(valueOf(5)));
        assertTrue(App.areAllDigitsUniq(valueOf(-89)));
        assertTrue(App.areAllDigitsUniq(valueOf(135)));
        assertTrue(App.areAllDigitsUniq(valueOf(15974)));
        assertTrue(App.areAllDigitsUniq(valueOf(-321654)));

        assertFalse(App.areAllDigitsUniq(valueOf(55)));
        assertFalse(App.areAllDigitsUniq(valueOf(-998)));
        assertFalse(App.areAllDigitsUniq(valueOf(1234067890)));
        assertFalse(App.areAllDigitsUniq(valueOf(-4556677)));
    }

    @Test
    public void isAllDigitsTheSame_nullArguments_ThrowsException() {
        assertThrows(NullPointerException.class,
                () -> App.isAllDigitsTheSame(null));
    }

    @Test
    public void isAllDigitsTheSame_validInput_returnsWhatExpected() {
        assertTrue(App.isAllDigitsTheSame(valueOf(5)));
        assertTrue(App.isAllDigitsTheSame(valueOf(-1)));
        assertTrue(App.isAllDigitsTheSame(valueOf(0)));
        assertTrue(App.isAllDigitsTheSame(valueOf(77)));
        assertTrue(App.isAllDigitsTheSame(valueOf(-999)));

        assertFalse(App.isAllDigitsTheSame(valueOf(10)));
        assertFalse(App.isAllDigitsTheSame(valueOf(-50)));
        assertFalse(App.isAllDigitsTheSame(valueOf(11111110)));
        assertFalse(App.isAllDigitsTheSame(valueOf(-33333337)));
        assertFalse(App.isAllDigitsTheSame(valueOf(-1111101111)));
        assertFalse(App.isAllDigitsTheSame(valueOf(15481)));
    }

    private static List<BigInteger> asList(Integer ... nums) {
        return Arrays.stream(nums).map(BigInteger::valueOf)
                .collect(Collectors.toList());
    }
}