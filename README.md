## Java Fundamentals. Task #2
Можно запустить проект через Maven:
```
mvn compile exec:java
```
Или запустить уже запакованный .jar:
```
mvn package
java -jar target/java-fundamentals-task-2-1.0-SNAPSHOT.jar
```
Необходима версия Java 1.8 или выше.

## Общие требования
1. Код приложения должен быть отформатирован в едином стиле и соответствовать Java Code Convention.
2. Если приложение содержит консольные меню или ввод/вывод, то они должны быть минимальными, достаточными и интуитивно понятными. Язык – английский.
3. Приложение должно быть работоспособным - т.е. запускаться без дополнительных манипуляций.
4. Результат каждой операции, которую требуется вывести в консоль должен выводится на новую строку.

## Задание
Создать консольное приложение реализующее следующий функционал:

1. Приложение позволяет ввести n чисел с консоли. 
2. Найти самое короткое и самое длинное число, выводит найденные числа и их длину.
3. Найти число, в котором количество различных цифр минимально. Если таких чисел несколько, находит первое из них и вывести в консоль. 
4. Найти число, цифры в котором идут в строгом порядке возрастания. Если таких чисел несколько, найти первое из них и вывести в консоль. 
5. Найти число, состоящее только из различных цифр. Если таких чисел несколько, найти первое из них и вывести в консоль. 

